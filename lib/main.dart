import 'package:flutter/material.dart';
import 'package:flutter_stripe_app/src/pages/home_page.dart';
import 'package:flutter_stripe_app/src/pages/pago_completo_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'home_page',
      routes: {
        'home_page': (_) => HomePage(),
        'pago_completo': (_) => PagoCompletoPage()
      },
      theme: ThemeData.light().copyWith(
          primaryColor: Color(0xff284879),
          scaffoldBackgroundColor: Color(0xff21232A)),
    );
  }
}
