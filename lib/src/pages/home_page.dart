import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_widget.dart';
import 'package:flutter_stripe_app/src/data/tarjetas.dart';
import 'package:flutter_stripe_app/src/helpers/helpers.dart';
import 'package:flutter_stripe_app/src/pages/tarjeta_page.dart';
import 'package:flutter_stripe_app/src/widgets/btn_total_pay.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          title: Text('Pagar'),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () async {
                  //
                  // mostrarLoading(context);
                  // await Future.delayed(Duration(seconds: 1));
                  // //Despues de un segundo el loading desaparece.
                  // Navigator.pop(context);

                  mostrarAlerta(context, 'kamarena', 'lola');
                })
          ],
        ),
        body: Stack(
          children: <Widget>[
            //
            Positioned(
              width: size.width,
              height: size.height,
              top: 200,
              child: PageView.builder(
                controller: PageController(viewportFraction: 0.9),
                itemCount: tarjetas.length,
                itemBuilder: (_, i) {
                  //
                  final tarjeta = tarjetas[i];

                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context, navegarFadeIn(context, TarjetaPage()));
                    },
                    child: Hero(
                      tag: tarjeta.cardNumber,
                      child: CreditCardWidget(
                        cardNumber: tarjeta.cardNumberHidden,
                        expiryDate: tarjeta.expiracyDate,
                        cardHolderName: tarjeta.cardHolderName,
                        cvvCode: tarjeta.cvv,
                        showBackView: false,
                      ),
                    ),
                  );
                },
              ),
            ),

            Positioned(bottom: 0, child: ButtonTotalPay())
          ],
        ));
  }
}
